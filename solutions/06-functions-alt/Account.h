#pragma once

typedef enum {
    CHECK, CREDIT
} AccountType;

typedef struct {
    AccountType type;
    int         balance;
    float       rate;
} Account;


extern Account* account_init(Account* acc, AccountType type, int balance, float rate);

extern void     accounts_init(Account accounts[], unsigned size);
extern void     accounts_print(Account accounts[], unsigned size);
extern void     accounts_update(Account accounts[], unsigned size);

