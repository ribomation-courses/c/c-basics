#!/usr/bin/env bash
set -eu

OUT=./build
CFLAGS="-std=c11 -Wall -Wextra -Werror -Wfatal-errors"

LIBNAME=account
LIBFILE=lib${LIBNAME}.a
LIBDIR=${OUT}
APP=${OUT}/bank

set -x
rm -rf ${OUT}
mkdir -p ${OUT}/{libobj,appobj}

echo 'Building the AR file...'
gcc ${CFLAGS} -c Account.c -o ${OUT}/libobj/Account.o
gcc ${CFLAGS} -c Node.c -o ${OUT}/libobj/Node.o
gcc ${CFLAGS} -c List.c -o ${OUT}/libobj/List.o
ar crs ${LIBDIR}/${LIBFILE} ${OUT}/libobj/*.o
ar tv ${LIBDIR}/${LIBFILE}

echo 'Building the APP'
gcc ${CFLAGS} -c bank-support.c -o ${OUT}/appobj/bank-support.o
gcc ${CFLAGS} -c bank.c -o ${OUT}/appobj/bank.o
gcc ${OUT}/appobj/*.o -L${LIBDIR} -l${LIBNAME} -o ${APP}

echo 'Listing the contents of ./build/'
tree ${OUT}

echo 'Running the APP'
${APP} 3
