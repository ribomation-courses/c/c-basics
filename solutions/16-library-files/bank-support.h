#pragma once

typedef Balance (* AccountTransformer)(Account*);

void populate(List* accounts, int numAccounts);
void print(List* accounts);
void drain(List* accounts);
void apply(List* accounts, AccountTransformer f);

