cmake_minimum_required(VERSION 3.10)
project(09_dynamic_memory C)

set(CMAKE_C_STANDARD 99)

add_executable(dynamic-ints dynamic-ints.c)
add_executable(dynamic-accounts Account.h Account.c dynamic-accounts.c)

