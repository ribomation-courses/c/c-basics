#pragma once

typedef struct {
    int   balance;
    float rate;
} Account;

extern Account*     account_init(Account* self, int balance, float rate);
extern void         account_print(Account* self);
