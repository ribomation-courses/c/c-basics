#include <stdio.h>
#include <stdlib.h>

void single_int_calloc() {
    int* ptr = (int*) calloc(1, sizeof(int));
    printf("[single_int_calloc] ptr=%p, *ptr=%d\n", ptr, *ptr);
    *ptr = 42424242;
    printf("[single_int_calloc] ptr=%p, *ptr=%d\n", ptr, *ptr);
    free(ptr);
    printf("[single_int_calloc] ptr=%p, *ptr=%d\n", ptr, *ptr);
}

void single_int_malloc() {
    int* ptr = (int*) malloc(sizeof(int));
    printf("[single_int_malloc] ptr=%p, *ptr=%d\n", ptr, *ptr);
    *ptr = 123456789;
    printf("[single_int_malloc] ptr=%p, *ptr=%d\n", ptr, *ptr);
    free(ptr);
    printf("[single_int_malloc] ptr=%p, *ptr=%d\n", ptr, *ptr);
}

void int_array(unsigned size) {
    printf("--- int array ---\n");
    int* arr = (int*) calloc(size, sizeof(int));
    for (int k = 0; k < size; ++k)
        arr[k] = k + 1;

    for (int k = 0; k < size; ++k)
        printf("arr[%d] = %d\n", k, arr[k]);

    int sum = 0;
    for (int k = 0; k < size; ++k)
        sum += arr[k];
    printf("SUM(1..%d) = %d\n", size, sum);

    free(arr);
}

int main() {
    single_int_calloc();
    single_int_malloc();
    int_array(10);
    return 0;
}

