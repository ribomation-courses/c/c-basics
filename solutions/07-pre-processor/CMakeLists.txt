cmake_minimum_required(VERSION 3.7)
project(07_pre_processor)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_FLAGS "-Wall -Wextra -Werror -Wfatal-errors")

add_executable(minmax       defs.h minmax.c)
add_executable(cppbug       cppbug.c)
add_executable(cppbug-fix   cppbug-fix.c)
add_executable(cppbug-fix-2   cppbug-fix-2.c)
