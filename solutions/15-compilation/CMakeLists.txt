cmake_minimum_required(VERSION 3.10)
project(15_compilation LANGUAGES C)

set(CMAKE_C_STANDARD 99)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(math math-app.c)
target_link_libraries(math m)
