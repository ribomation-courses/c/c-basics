cmake_minimum_required(VERSION 3.10)
project(pointers)

set(CMAKE_C_STANDARD 11)
add_compile_options(-Wall -Wextra -Werror -Wfatal-errors)

add_executable(indirect-int         indirect-int.c)
add_executable(indirect-array       indirect-array.c)
add_executable(single-account       single-account.c)
add_executable(array-account        array-account.c)
add_executable(account-functions    account-functions.c)
add_executable(interest-computation interest-computation.c)
add_executable(accounts             account.h account.c bank.c)
