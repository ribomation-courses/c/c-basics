#include <stdio.h>

typedef struct {
    int   balance;
    float rate;
    char filler;
} Account;

int main() {
    Account        arr[] = {
            {1000, 0.25, 'a'},
            {2000, 0.5, 'a'},
            {3000, 0.75, 'a'},
            {4000, 1.0, 'a'},
            {5000, 1.25, 'a'},
            {6000, 1.55, 'a'},
    };
    const unsigned N     = sizeof(arr) / sizeof(arr[0]);
//    const Account* END = &arr[N];
    const Account* END = arr + N;

    for (Account* p = &arr[0]; p != END; ++p) {
        printf("acc: {SEK %d, %.2f%%}\n", p->balance, p->rate);
    }

    for (Account* p = (arr + 0); p != END; ++p) {
        p->balance = (int) (p->balance * (1 + p->rate / 100));
    }

    printf("----------\n");
    for (Account* p = arr; p != (arr + N); ++p) {
        printf("acc: {SEK %d, %.2f%%}\n", p->balance, p->rate);
    }

    size_t        sz1 = sizeof(Account);
    unsigned long sz2 = sizeof(Account);
    unsigned int  sz3 = sizeof(Account);
    printf("sz1=%ld sz2=%ld sz3=%d\n", sz1, sz2, sz3);

    printf("arr=%ld, arr+1=%ld, diff=%ld\n",
           (size_t) arr, (size_t) (arr + 1), ((size_t)(arr + 1) - (size_t)arr));

    return 0;
}
