#include <stdio.h>

typedef struct {
    int   balance;
    float rate;
} Account;

Account*    account_init(Account* self, int balance, float rate);
void        account_print(Account* self);

int main() {
    Account acc;
    account_init(&acc, 500, 1.25);
    account_print(&acc);

    printf("----------\n");
    Account arr[5];
    const unsigned N = sizeof(arr) / sizeof(arr[0]);

    for (unsigned k = 0; k < N; ++k) {
        account_init(&arr[k], (k + 1) * 1000, (float) ((k + 1) * 0.25));
    }

    for (unsigned k = 0; k < N; ++k) {
        account_print(arr + k);
    }

    return 0;
}


Account* account_init(Account* self, int balance, float rate) {
    self->balance = balance;
    self->rate = rate;
    return self;
}

void account_print(Account* self) {
    printf("Account{SEK %d, %.2f%%}\n", self->balance, self->rate);
}
