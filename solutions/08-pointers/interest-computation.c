#include <stdio.h>
#include <memory.h>

typedef struct {
    int   balance;
    float rate;
} Account;

Account*    account_init(Account* self, int balance, float rate);
void        account_print(Account* self);
void        account_final_balance(Account* self);
void        account_initial_balance(Account* self);

typedef void (* AccountFunction)(Account*);


void func() {
    Account acc;
    memset(&acc, 0, sizeof(Account));
    account_print(&acc);
}

int main() {
    func();
    printf("this is a dummy print stmt arg1=%s, arg2=%d, arg3=%.3f\n",
            "arg #1", 424242, 3.1415926);
    func();
    printf("----------\n");

    Account acc;
    account_print(&acc);

    account_init(&acc, 1000, 3.5);
    account_print(&acc);

    account_final_balance(&acc);
    account_print(&acc);

    account_initial_balance(&acc);
    account_print(&acc);

    printf("----------\n");
    AccountFunction arr[] = {
            &account_final_balance,
            &account_initial_balance,
    };
    const unsigned N = sizeof(arr) / sizeof(arr[0]);

    account_init(&acc, 10000, 2.5);
    account_print(&acc);
    printf("----------\n");
    for (unsigned k = 0; k < N; ++k) {
        (*arr[k])(&acc);
        account_print(&acc);
    }

    return 0;
}


Account* account_init(Account* self, int balance, float rate) {
    self->balance = balance;
    self->rate    = rate;
    return self;
}

void account_print(Account* self) {
    printf("Account{SEK %d, %.2f%%}\n", self->balance, self->rate);
}

void account_final_balance(Account* self) {
    self->balance = (int) (self->balance * (1 + self->rate / 100));
}

void account_initial_balance(Account* self) {
    self->balance = (int) (self->balance / (1 + self->rate / 100));
}
