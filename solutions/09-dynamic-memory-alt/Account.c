
#include <stdio.h>
#include "Account.h"


Account* account_init(Account* self, int balance, float rate) {
    self->balance = balance;
    self->rate    = rate;
    return self;
}

void account_print(Account* self) {
    printf("Account{SEK %d, %.2f%%}\n", self->balance, self->rate);
}

