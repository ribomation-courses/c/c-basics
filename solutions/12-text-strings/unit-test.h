#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

#define streq(lhs, rhs)     (strcmp(lhs, rhs) == 0)
#define strneq(lhs, rhs)    (!streq(lhs, rhs))

#define TEST(func, payload, expected) {                         \
        char* actual = func(payload);                           \
        printf("%s(%s) -> [%s]\n", #func, payload, actual);     \
        if (strneq(actual, expected)) {                         \
            printf("*** Failure: expected [%s]\n", expected);   \
            printf("%s:%d\n", __FILE__, __LINE__);              \
            exit(1);                                            \
        }                                                       \
        free(actual);                                           \
    }

#define TEST2(func, lhs, rhs, expected) {                       \
        char* actual = func(lhs, rhs);                          \
        printf("%s(%s, %s) -> [%s]\n", #func, lhs, rhs, actual);\
        if (strneq(actual, expected)) {                         \
            printf("*** Failure: expected [%s]\n", expected);   \
            printf("%s:%d\n", __FILE__, __LINE__);              \
            exit(1);                                            \
        }                                                       \
        free(actual);                                           \
    }
