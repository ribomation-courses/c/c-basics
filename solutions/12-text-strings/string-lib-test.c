#include <stdio.h>

#include "unit-test.h"
#include "string-lib.h"

int main() {
    const char* sample = "Hello World 123!";
    
    TEST(toUpperCase, sample, "HELLO WORLD 123!");
    TEST(toLowerCase, sample, "hello world 123!");
    TEST(reverse, sample, "!321 dlroW olleH");
    TEST(reverse2, sample, "!321 dlroW olleH");
    TEST(reverse2, "abc", "cba");
    TEST(reverse2, "abcd", "dcba");
    TEST2(concat, "foo", "bar", "foobar");

    printf("all tests succeeded\n");
    return 0;
}
