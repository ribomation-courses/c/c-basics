#include <stdio.h>
#include <assert.h>
#include "Account.h"
#include "List.h"

void populate(List* accounts, int numAccounts) {
    InterestRate   rates[] = {0.5, 1.5, 2, 2.5, 3};
    const unsigned R       = sizeof(rates) / sizeof(InterestRate);

    while (--numAccounts >= 0) {
        Balance      amt        = (numAccounts + 1) * 1000 & 99999;
        InterestRate percentage = rates[numAccounts % R];
        list_push(accounts, account_new(amt, percentage));
    }
}

void print(List* accounts) {
    int accno = 1;
    for (Node* node = accounts->first; node != NULL; node = node->next) {
        printf("%4d) ", accno++);
        account_print(node->data);
    }
}

void drain(List* accounts){
    while (!list_empty(accounts)) {
        account_dispose(list_pop(accounts));
    }
}

int main(int numArgs, char* args[]) {
    int numAccounts = numArgs > 1 ? atoi(args[1]) : 42;

    List accounts;
    list_init(&accounts);
    populate(&accounts, numAccounts);
    assert(list_size(&accounts) == numAccounts);

    print(&accounts);
    drain(&accounts);
    assert(list_empty(&accounts));

    return 0;
}
