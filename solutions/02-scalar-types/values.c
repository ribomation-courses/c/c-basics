#include <stdio.h>

int main() {
    printf("Integer: %d\n", 42);
    printf("Floating-Point: %.4f\n", 3.1415926);
    printf("Character: %c\n", 'A');
    printf("Text: %s\n", "Tjolla Hopp");
    return 0;
}
