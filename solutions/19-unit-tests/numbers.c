#include "numbers.h"

long fib(int n) {
    if (n <= 0) return 0;
    if (n == 1) return 1;
    return fib(n - 2) + fib(n - 1);
}

long sum(int n) {
    return n * (n + 1) / 2;
}

long factorial(int n) {
    if (n <= 0) return 0;
    if (n == 1) return 1;
    return n * factorial(n - 1);
}
